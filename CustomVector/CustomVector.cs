using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

/**
* <summary>
* A Vector3 class made for Heavy Vehicle Simulator in order to ease Heavy Vehicle Simulator Programmers into creating a json for Vector3.
* <br>Created by Izzan A. Fu'ad</br> 
* </summary>
*/
[Serializable]
public class CustomVector
{
    [JsonIgnore][SerializeField] private float x;
    [JsonIgnore][SerializeField] private float y;
    [JsonIgnore][SerializeField] private float z;

    public float X { get => x; set => x = value; }
    public float Y { get => y; set => y = value; }
    public float Z { get => z; set => z = value; }

    /**
    * <summary>
    * Returns this vector with a magnitude of 1 (Read Only).
    * </summary>
    */
    [JsonIgnore]
    public CustomVector Normalized
    {
        get
        {
            float length = Magnitude;
            if (length == 0)
                return this;

            x /= length;
            y /= length;
            z /= length;

            return new(x, y, z);
        }
    }

    [JsonIgnore]
    public float Magnitude => Mathf.Sqrt((x * x) + (y * y) + (z * z));

    public CustomVector() { }
    public CustomVector(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public CustomVector(Vector3 vector3)
    {
        x = vector3.x;
        y = vector3.y;
        z = vector3.z;
    }
    public CustomVector(Vector2 vector2)
    {
        x = vector2.x;
        y = vector2.y;
        Z = 0;
    }

    public static CustomVector zero = new(0, 0, 0);
    public static CustomVector right = new(1, 0, 0);
    public static CustomVector left = new(-1, 0, 0);
    public static CustomVector up = new(0, 1, 0);
    public static CustomVector down = new(0, -1, 0);
    public static CustomVector forward = new(0, 0, 1);
    public static CustomVector back = new(0, 0, -1);

    public double DistanceTo(CustomVector toVector)
    {
        return GetDistance(this, toVector);
    }

    public double AngleTo(CustomVector toVector)
    {
        return GetAngle(this, toVector);
    }

    public CustomVector LerpTo(CustomVector toVector, float time)
    {
        return Lerp(this, toVector, time);
    }

    private Vector3 ToVector3()
    {
        return new(x, y, z);
    }

    public Quaternion ToQuaternion()
    {
        return Quaternion.Euler(x, y, z);
    }

    public override string ToString()
    {
        return ToString(this);
    }

    public void Load(CustomVector copy)
    {
        x = copy.X;
        y = copy.Y;
        z = copy.Z;
    }

    public static float Dot(CustomVector fromVector, CustomVector toVector)
    {
        return (fromVector.X * toVector.X) + (fromVector.Y * toVector.Y) + (fromVector.Z * toVector.Z);
    }

    public static float GetDistance(CustomVector fromVector, CustomVector toVector)
    {
        float fx = Mathf.Pow(fromVector.X - toVector.X, 2);
        float fy = Mathf.Pow(fromVector.Y - toVector.Y, 2);
        float fz = Mathf.Pow(fromVector.Z - toVector.Z, 2);

        return Mathf.Sqrt(fx + fy + fz);
    }

    public static float GetAngle(CustomVector fromVector, CustomVector toVector)
    {
        float dotProduct = Dot(fromVector, toVector);
        float magnitudeProduct = fromVector.Magnitude * toVector.Magnitude;
        return Mathf.Acos(dotProduct / magnitudeProduct);
    }

    public static CustomVector MoveTowards(CustomVector fromVector, CustomVector toVector, float maxDistanceDelta)
    {
        CustomVector delta = toVector - fromVector;
        float distance = delta.Magnitude;

        return distance <= maxDistanceDelta || distance == 0 ? toVector : fromVector + (delta * (maxDistanceDelta / distance));
    }

    public static CustomVector Lerp(CustomVector fromVector, CustomVector toVector, float time)
    {
        time = Mathf.Clamp01(time);
        return fromVector + ((toVector - fromVector) * time);
    }

    public static string ToString(CustomVector json)
    {
        return JsonConvert.SerializeObject(json, Formatting.Indented);
    }

    public static CustomVector FromJson(string json)
    {
        return JsonConvert.DeserializeObject<CustomVector>(json);
    }

    public static implicit operator Vector3(CustomVector vector3)
    {
        return vector3.ToVector3();
    }

    public static implicit operator CustomVector(Vector3 vector3)
    {
        return new(vector3);
    }

    public static implicit operator Quaternion(CustomVector vector3)
    {
        return vector3.ToQuaternion();
    }

    public static implicit operator CustomVector(Quaternion quaternion)
    {
        return quaternion.eulerAngles;
    }

    public static CustomVector operator +(CustomVector fromVector, CustomVector toVector)
    {
        CustomVector result = new()
        {
            X = fromVector.X + toVector.X,
            Y = fromVector.Y + toVector.Y,
            Z = fromVector.Z + toVector.Z,
        };
        return result;
    }

    public static CustomVector operator +(CustomVector fromVector, float fromFloat)
    {
        CustomVector result = new()
        {
            X = fromVector.X + fromFloat,
            Y = fromVector.Y + fromFloat,
            Z = fromVector.Z + fromFloat,
        };
        return result;
    }

    public static CustomVector operator -(CustomVector fromVector, CustomVector toVector)
    {
        CustomVector result = new()
        {
            X = fromVector.X - toVector.X,
            Y = fromVector.Y - toVector.Y,
            Z = fromVector.Z - toVector.Z,
        };
        return result;
    }

    public static CustomVector operator -(CustomVector fromVector, float fromFloat)
    {
        CustomVector result = new()
        {
            X = fromVector.X - fromFloat,
            Y = fromVector.Y - fromFloat,
            Z = fromVector.Z - fromFloat,
        };
        return result;
    }

    public static CustomVector operator *(CustomVector fromVector, CustomVector toVector)
    {
        CustomVector result = new()
        {
            X = fromVector.X * toVector.X,
            Y = fromVector.Y * toVector.Y,
            Z = fromVector.Z * toVector.Z,
        };
        return result;
    }

    public static CustomVector operator *(CustomVector fromVector, float fromFloat)
    {
        CustomVector result = new()
        {
            X = fromVector.X * fromFloat,
            Y = fromVector.Y * fromFloat,
            Z = fromVector.Z * fromFloat,
        };
        return result;
    }

    public static CustomVector operator /(CustomVector fromVector, CustomVector toVector)
    {
        CustomVector result = new()
        {
            X = fromVector.X / toVector.X,
            Y = fromVector.Y / toVector.Y,
            Z = fromVector.Z / toVector.Z,
        };
        return result;
    }

    public static CustomVector operator /(CustomVector fromVector, float fromFloat)
    {
        CustomVector result = new()
        {
            X = fromVector.X / fromFloat,
            Y = fromVector.Y / fromFloat,
            Z = fromVector.Z / fromFloat,
        };
        return result;
    }

    public static bool operator ==(CustomVector fromVector, CustomVector toVector)
    {
        return fromVector.x == toVector.x &&
               fromVector.y == toVector.y &&
               fromVector.z == toVector.z;
    }

    public static bool operator !=(CustomVector fromVector, CustomVector toVector)
    {
        return fromVector.x != toVector.x ||
               fromVector.y != toVector.y ||
               fromVector.z != toVector.z;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
            return false;

        CustomVector toVector = (CustomVector)obj;
        return this == toVector;
    }

    public override int GetHashCode()
    {
        return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
    }
}
