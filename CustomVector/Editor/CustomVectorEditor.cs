using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(CustomVector))]
public class CustomVectorEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        EditorGUI.indentLevel = 0;

        float x = property.FindPropertyRelative("x").floatValue;
        float y = property.FindPropertyRelative("y").floatValue;
        float z = property.FindPropertyRelative("z").floatValue;

        float[] values = new float[] { x, y, z };
        string[] labels = new string[] { "X", "Y", "Z" };

        for (int i = 0; i < 3; i++)
        {
            Rect rect = new(position.x + (i * 60), position.y, 50, position.height);
            EditorGUIUtility.labelWidth = 10;
            EditorGUIUtility.fieldWidth = 40;
            values[i] = EditorGUI.FloatField(rect, labels[i], values[i]);
        }

        property.FindPropertyRelative("x").floatValue = values[0];
        property.FindPropertyRelative("y").floatValue = values[1];
        property.FindPropertyRelative("z").floatValue = values[2];

        EditorGUI.EndProperty();
    }
}